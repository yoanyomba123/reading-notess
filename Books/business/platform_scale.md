# Platform Scale
---
## Preface
Software is eating the world. - marc andreessen
Software is eating media, telecom, professional services, retial, banking, healthcare, education, energy, transportation. 
Softwares ability to “to orchestrate people and resources, make intelligent decisions, and enable a connected global workforce to create value that is the real force driving disruption today”

The democratization of connectivity and the rise of data-driven decision making systems are leading to an emergence of a new range of business models. 

The world is now in the business of enabling efficient social and business interactions mediated by software. Such system foloow the platform business model. 

Leveraging technologies often commoditized to ochestrate connected users toward new and efficient value creating interactions holds the key to the business models of the future. 

Business success and failure of digital business models in a networked world.

----

### Chapter 1
We are not in the business of building software, we are in the business of enabling interactions  
#### Pipes to platforms: A shift in business design
The internet restructured the mechanics by which businesses create and deliver value. Business models have moved from pipes to platforms. 
In a piped based business model, value is created upstream and consumed downstream creating a linear flow of value. 

Three forces today however have challenged the piped based business model
- increseased connectivity
- decentralized production
- artificial intelligence

The largest players in the world favor the platform business model creating a plug and play infrastructure that enables producers and consumers of value to connect and interact with each other in novel manners. Platforms foster the co-creation and exchange of value amongst users. Platforms functionalities can be extended through api while platform uses act as producers for others to consume. 

Platforms allow usrs to exchange value amongst each other while on the contrary pipse create and push value to consumers. 
 Platforms govern the interactions that ensue once participants come onboard the platform. The rise of platforms is s proven indication that we are in the midst of a fundamental change in the very design of business which is associated to the multi-directional flow of value amongst participants. 

#### Pipes to platforms: three primary shifts
1. shift in markets from consumers to producers
In a traditional business context the consumer's relationship with the business was straight forward meaning the consumer was located at the end of the pipe. On Platforms the business does not create the end value rather, it enables value creation. Now platform participants take a production and consumer role. 

2. shift in competitive advantage from resources to ecosystems
Pipes competed through resource ownership and control leading to popularity in the vertically integrated business as well as scaling through mergers and acquisitions. In the world of piped based businesses, firms competed on a basis of control over internal resources ownership and IP. Platform based businesses are successful not because of the amount of resources they withold, rather, because ot the ecosystem of producers and consumers that they succeed in attracting, curating, and cultivating.  Ecosystems are the key enablers of value creation on platforms and a new source of competitive advantage. Platforms use data to orchestrate physical and digital resources across their respective ecosystems 

3. Shift in value creation from processes to interactions
Platforms focus on matching the right content to the right user. In linear pipes, value is created and centered on an end-to-end process that shifts value down the pipe from producer to consumer. The interaction between producers and consumers on platforms facilitated by the platform itself determines the creation of value and its exchange. Platforms enable value creation and exchange by matching the most relevant resources from producers in the ecosystem with the consumers on the platform needing thoses resource.
Process focus vs Interaction focus 

#### History of Scale
The ability of a business to scale is determined by its ability to aggregate the inputs to business, labor and resources, and coordinate them efficiently toward the creation of value and its delivery. 

##### Pipe scale
Business scale powerd by the ability to coordinate internal labor and resources toward efficient value creation and toward delivery of the created value to an aggregated consumer base. 

So business were able to scale by aggregating labor and resources internally, and using value creating business processes to transform these inputs into functioning products and deliverable services.

###### Platform Scale
Business scale powered by the ability to leverage and orchestrate a global conencted ecosystem of producers and consumers toward efficient value creationa and exchange. The management of platform scale involves the design and optimization of value-exchange interactions between producers and consumers. 


#### Manifestations of platform scale
universal shift from linear to more networked business models. 
##### Social media- pipes give way to social participation
The democratizatio of content production tools and the shift in media distribution power from journalists to user-producers has led to the shift from traditional media to social media.  New reliance on the ability to orchestration social interactions between producers and consumers. 

##### The on-demand economy - service delivery on platforms
Hotels leverage pipe scale. Airbnb solves the same needs leveraging platform scale. Value lies in managing the echange of services instead of owning resources. Platforms like aribnb scale an ecosystem of service providers which are mostly distributed and autonomous. Platforms such as airbnb invest in creating better trust mechanisms that identify and differentiate good behavior from poor behavior and minimize interaction risks while hotels invest in resource creation. 

##### The app economy- leveraging platforms for innovation
Apple and android change the rules by using a networked platform to disrupt a controlled pipe. External developers plug into the platform and create apps on top of it. Consumers moved to platform phones from nokia and blackberry since functionalities could easily be extended by apps. Firms must leverage platforms for innovation

##### The intelligent internet of things
As we move from pipes to platforms, the business model of consumer goods will move from one centered on product sales to one centered on platform enabled connected services where products work as part of an ecosystem.  Nike's fuelband have changed it from a company selling shoes to a company working to unlock new engagement and monetization using a data platform. 

##### Products and services as platform powered communities
Instagram aggregates the world's photography community while also aggregating the community'sattention for commerce. Today's products and services benefit from platform powered communities. 

##### 3D Printing - the distributed factory
With the advent of 3D printing, there is a huge indication that some forms of manufacturing will move from pipes to platforms leading to the creation of entirely new markets. 

##### Crowdsourcing and wikipedia everything
“The coordination of production has traditionally required a supply chain of integrated, top-down processes and controls. Wikipedia reconfigured this linear process and allowed it to be managed cyclically on a network.”
“Waze, an Israeli traffic prediction app, crowdsources driving information from multiple drivers while simultaneously using algorithms to determine authenticity before distributing traffic conditions to the wider community.
Wikipedia and Waze reimagine the organization of the traditional production function, away from supply chains and onto platforms. They provide an early glimpse into a future where value creation may not need a supply chain, instead being orchestrated via a network of connected users on a platform.”

##### CryptoCurrencies
“Platform theory helps to explain the workings of cryptocurrencies, like Bitcoin. Decentralized management – through mechanisms like the blockchain – has the potential to change governance structures for the next generation of platforms, much like social feedback tools power curation on many of the current generation of platforms. ”

#### Platform Scale Imperative
“At their core, platforms enable a plug-and-play business model. Other businesses can easily connect their business with the platform, build products and services on top of it, and co-create value. Platforms primarily benefit not from internal production but from a wider source of open co-creation and open market interactions. This ability to drive interactions through a “plug-and-play” infrastructure is a defining characteristic of platform scale.”

## References
Excerpt From: Choudary, Sangeet Paul. “Platform Scale: How an emerging business model helps startups build large empires with minimum investment.” Apple Books.
