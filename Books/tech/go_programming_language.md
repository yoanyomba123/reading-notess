# Go Programming Language
---
### Introduction
Go is simialr to C and achieves maximum effect with minimum means. Facilitates concurrency, provides a better data abstraction model, and OOP, and has automatic memory management and garbage collection.

Go is useful in building networked servers and since it is compiled and has typed errors, it not only runs faster than most other applications implemented in other languages but is less prone to failure.

Simplicity is the key to good software as complexity is multiplicative

Simpler type system in present in go. Go's aggregate types hold elements directly requiring less memory and storage.

No inheritance, rather, composition is favored.

---

### Chapter 1
 #### Section 1.5 (Fetching a Url)
go provides collections of packages grouped together to obtain information on the internet, make low level connections, and set up servers.
`http.Get(url)` from the net/http package
##### Section 1.6 (Concurrent URL Fetch)
```go
ch := make(chan string) // channel creation
for _, url := range os.Args[1:]{
    go fetch(url, ch); // go routines
}
```
go routines communicate through channels which is created from the make command
Go can also be used to createweb servers. Web servers would be based on the net/http library.
`http.HandleFunc("/". handler) // handles each request` and is presnt within the mai function
`http.listenAndServer("localhost:8000", nil) // listens for incoming requests on port 8000

```go
func handler(w http.ResponseWriter, r *http.Request){
    fmt.printf(w, "Url.Path = %q\n", r.URL.Path)
 } // serves as the handler functions```
```
To avoid race conditions in backend logic, there must be an ensured mechanism allowing only one goroutine access to a specific variable at a time.
`mu.Lock() and mu.Unlock()`

##### Section 1.7 (To Be Addressed)
control flow - if, for, switch
Type declaration
Pointers (& = memory address of var. and * = value at pointer memory address)
Interfaces & Methods
Packages
Go Doc for documentation
---

### Chapter 2
#### Section 2.1 (Names)
if an entity is declared within a function it is local to the function and if declared outisde of the function then it is visible to all files of the package to which it belongs to. Case of first letter of a name determines its visibility across package boundaries.

If a name begins with an uppercase values, the the entity is exported and visible outside its own package definition. Package names themselves are alwyas lower case.

#### Section 2.2
4 major types of declarations, var, const, type, and func.
- package declaration --> import declaration --> package level declarations (types, vars, constants, functions

##### 2.3
var name type = expression
type or expression can be omitted in var declaration but not both.
Short variable definitions follow the following scheme
name := expression
short variable initialization both declare and initialize a given variable
:= is a declaration while = is an assignment. Very Important to keep this in mind

##### 2.4
a pointer is the address of some variable. Every variable has a memory address.
& defines the memory at which a variable is stored and * defines the value at that memory address.
the commmand new(type) is used to initialize and declare a variable of some type, initialize it to 0, and return its memory address.

##### 2.5
Type declaration provide the programmer with which characteristics and variable can take on.
Typically, defining a type is presented in the following manner
type name underlying-type

##### 2.6
packages = libraries
package serves as separate namespace for its declaration
Functions defined in differnet files but with the same package declaration can be referenced in similar ways.
It is important to use the goimport tool as it automatically inserts and removes packages from the import declaration

---

### Chapter 3
- Omit: Skip Since Only Mention Of Data Types (int, float, bools, etc)
Go Types fall into 4 basic categories
    - basic types, aggregate types, reference types, and interface types
##### 3.3
Complex Numbers (complex64 and complex128)
math/cmplx - complex number library
##### 3.5
String are immutable sequences of bytes and may contain some arbitrary data
To obtain characters within strings use the following index manner string[1:1]
String comparison == and < is performed byte by byte
Go source files are encoded in UTF-8
4 Standard packages are important when manipulating string: bytes, strings, strconv, unicode
To convert from integer to string it is important to use the following ```go strconv.Itoa(x)```

##### 3.6
constants are expressions whose values are known to the compile and whose evaluation is guaranteed to occur at compile time and not run time.
constants can be used to define enumerations
```go
    type Weekday int

    const (
        Sunday Weekday = iota
        Monday
        Tuesday
    )
```
this declares Sunday to be 0 and monday to be 1
Untyped constants typically withold a greater degree of precision since there is a lack of commitment to any respective type.
```go var x float32 = math.Pi``` is less precise than ```go const pi = math.Pi```
only constants can be untyped

---
### Chapter 4
topic of this chapter is addressing composite types (arrays, slices, maps, structs)
arrays and structs are of fixed size while slices and maps are dynamic data structures
###### 4.1
arrays are seldomly used in go due to their fixed lenght while slices can vary
[...] used to specify the lenght of an array based on the number of initialized values
Note: important to pass large arrays by reference
```go
    func zero(ptr *[32]byte){
        for i := range ptr [
            ptr[i] = 0
        }
    }
```
[2]int32{1,2}
[...]int32{1,2,3,4,5}
##### 4.2
Slices represent variable length sequences whose elements all have the same Type
[]T
slices have a length, a capacity, and a pointer
    - length defines number of elements in the slice currently, capacity defines the max amount of elements the slice can withold and a pointer points to the location of the datastructure in memory
slicing beyond cap() causes a panic and slicing beyond len() extends the slices
cannot use the == operator to check for equality between slices. Must use the Bytes.Equal() function
Standard practice is to dissalow all slice comparisons except to nil

##### 4.3
maps in go are hashtables
map[K]V where K and V are types of the keys and values
to create a map can be done in the following manner
```go ages := make(map[string]int)```
map element is not a variable and thus we cannot obtain its address
Note: Must allocate a map before storing any entities in it
Cannot compare maps to one another
Maps can be efficiently used to store graph data (maps of maps)

##### 4.4
Structs are aggregate data types that groups zero or more named values of arbitrary types as single entities
```go type employee struct{
    ID int
    Name string
    Address string
    DoB string
    Position string
    Salary int
}
```
A struct cannot contain itself but may have a pointer to a reference of itself
When dealing with struct always use pointers and pass by reference

##### 4.5
marshalling meaning converting from go data type to json json.Marshal
unmarshalling decoding json and populating go data type json.Unmarshal
json Encoder and Decoder (streaming encoding and decoding)
---

### Chapter 5
Allows us to wrap a sequence of statements as a unit to be called elsewhere in a program.
```go
func name(paramter-list)(result-list){
    body
}
```
Function parameters are typically passed by value but passing an object's pointer, a slice, a map, a function, or a channel results in the parameter being a reference
It is important to note that although go has a pretty good garbage collector, you the programmer must take care to release any unused operating system resources such as open files and network connections explicitly.

functions can have multiple return values (error and some underlying data type)

##### 5.4
exception tend to entangle the description of an error with the control flow required to handle it

if a function returns an error, its the caller's responsibility to handle it and take appropriate action

###### 5.4.1
_Error Handling Strategies_
1. propagate the error so failure in a subroutines becomes a failure in the calling routine
```go
resp, err := http.Get(url)
     if err != nil {
         return nil, err
     }
}
```
you can also construct some new error message including various pieces of information like the error that occured and the url being parsed

```golang
doc, err := html.Parse(resp.Body)
     resp.Body.Close()
     if err != nil {
         return nil, fmt.Errorf("parsing %s as HTML: %v", url, err)
     }
```
fmt.Errorf formats an error message and returns a new error value
useful to build descriptive errors

Your error message should be deliberate and meaningful description of the problem with sufficient and relevant detail and also be very consistent so that errors returned by the same function or gorup of functions are similar in form.

2. Retry failed operation possibly with some delay and a limit on the number of retry events
```go
// WaitForServer attempts to contact the server of a URL.
     // It tries for one minute using exponential back-off.
     // It reports an error if all attempts fail.
     func WaitForServer(url string) error {
         const timeout = 1 * time.Minute
         deadline := time.Now().Add(timeout)
         for tries := 0; time.Now().Before(deadline); tries++ {
             _, err := http.Head(url)
             if err == nil {
                 return nil // success
             }
             log.Printf("server not responding (%s); retrying...", err)
             time.Sleep(time.Second << uint(tries)) // exponential back-off
         }
         return fmt.Errorf("server %s failed to respond after %s", url, timeout)
     }
```
Library errors should propagate their respective errors to the caller
3. Log error and then continue
```go
if err := Ping(); err != nil {
         log.Printf("ping failed: %v; networking disabled", err)
}
```
4. In rare cases errors can be completely ignored

##### 5.5
functions can accept other functions as inputs
##### 5.6
named functions can be declared only at the package level while function literal can be used to denote a funciton within any expression. It is usually written in a similar manner as a function declaration but without a name.
```go
 // squares returns a function that returns
     // the next square number each time it is called.
     func squares() func() int {
         var x int
         return func() int {
x++
return x * x }
}
     func main() {
         f := squares()
         fmt.Println(f()) // "1"
         fmt.Println(f()) // "4"
         fmt.Println(f()) // "9"
         fmt.Println(f()) // "16"
}
```
ananymous functions have access to the entire lexical environment. This allows the lifetime of a variable to be independent from its cope. Often time this method is referred as a _closure_.

```go
// Package links provides a link-extraction function.
     package links
     import (
         "fmt"
"net/http"
         "golang.org/x/net/html"
     )
     // Extract makes an HTTP GET request to the specified URL, parses
     // the response as HTML, and returns the links in the HTML document.
     func Extract(url string) ([]string, error) {
         resp, err := http.Get(url)
         if err != nil {
             return nil, err
         }
         if resp.StatusCode != http.StatusOK {
             resp.Body.Close()
             return nil, fmt.Errorf("getting %s: %s", url, resp.Status)
}
         doc, err := html.Parse(resp.Body)
         resp.Body.Close()
         if err != nil {
             return nil, fmt.Errorf("parsing %s as HTML: %v", url, err)
         }
         var links []string
         visitNode := func(n *html.Node) {
             if n.Type == html.ElementNode && n.Data == "a" {
                 for _, a := range n.Attr {
 } }
    if a.Key != "href" {
        continue
    }
    link, err := resp.Request.URL.Parse(a.Val)
    if err != nil {
        continue // ignore bad URLs
    }
    links = append(links, link.String())
}
    forEachNode(doc, visitNode, nil)
    return links, nil
}
```
above is an example use of annonymous functions

###### 4.6.1
Caveat
When performing loop iterations and making use of loop conditionals within a function, it is important to consider the fact that for loops introduce a new lexical block. All function values created by such a loop capture and share the same variable as an addressable storage location not its value at a respective iteration. Thus to work around this problem, an inner variable is often used to work around this problem
```go
var rmdirs []func()
     for _, d := range tempDirs() {
         dir := d               // NOTE: necessary!
         os.MkdirAll(dir, 0755) // creates parent directories too
         rmdirs = append(rmdirs, func() {
             os.RemoveAll(dir)
         })
}
     // ...do some work...
     for _, rmdir := range rmdirs {
         rmdir() // clean up
}
```

The incorrect way many people may apply this is present below

##### 4.7
Variadic functions are function that can be called with varying number of arguments. Below is a sample declaration of a variadic function.
```go
func sum(vals ...int) int {
         total := 0
         for _, val := range vals {
             total += val
}
         return total
     }
```

##### 4.8
defer statement is an ordinary function call prefixed by the keyword defer. The function and argument expression are evaluated when the statement is executed but the actual call is deffered until the function that contains the defer statement returns. If multiple function calls are deffered, they are executed in the reverse order in which they were deffered.
Always defer right after a resource has been acquired.
```go
func title(url string) error {
         resp, err := http.Get(url)
         if err != nil {
return err }
         defer resp.Body.Close()
         ct := resp.Header.Get("Content-Type")
         if ct != "text/html" && !strings.HasPrefix(ct, "text/html;") {
             return fmt.Errorf("%s has type %s, not text/html", url, ct)
         }
         doc, err := html.Parse(resp.Body)
         if err != nil {
             return fmt.Errorf("parsing %s as HTML: %v", url, err)
         }
         // ...print doc's title element...
return nil }
```
```go
package ioutil
     func ReadFile(filename string) ([]byte, error) {
         f, err := os.Open(filename)
         if err != nil {
             return nil, err
         }
         defer f.Close()
         return ReadAll(f)
     }
```
##### 5.9
panic errors occur in areas of code that naturally should never fail except under grave situations
```go
 func MustCompile(expr string) *Regexp {
         re, err := Compile(expr)
         if err != nil {
panic(err) }
return re }
```
panic erros typically crash a program.

##### 5.10
If the built in recover function is called within a deffered function and the function containing the defer statement is panicking, recover ends the current state of panic and returns the panic value. The panicking function does not continue where it left off but returns normally.

```go
func Parse(input string) (s *Syntax, err error) {
         defer func() {
             if p := recover(); p != nil {
                 err = fmt.Errorf("internal error: %v", p)
} }()
         // ...parser...
     }
```
Recover selectively, in order words recover from errors that were meant to be recovered from.
```go
 // soleTitle returns the text of the first non-empty title element
     // in doc, and an error if there was not exactly one.
     func soleTitle(doc *html.Node) (title string, err error) {
         type bailout struct{}
         defer func() {
             switch p := recover(); p {
             case nil:
                 // no panic
             case bailout{}:
                 // "expected" panic
                 err = fmt.Errorf("multiple title elements")
             default:
                 panic(p) // unexpected panic; carry on panicking
             }
}()
         // Bail out of recursion if we find more than one non-empty title.
         forEachNode(doc, func(n *html.Node) {
             if n.Type == html.ElementNode && n.Data == "title" &&
                 n.FirstChild != nil {
                 if title != "" {
                     panic(bailout{}) // multiple title elements
                 }
                 title = n.FirstChild.Data
             }
         }, nil)
         if title == "" {
             return "", fmt.Errorf("no title element")
         }
         return title, nil
     }
```
---

### Chapter 6
an object is a value or variable that has methods, methods are function associated with a respective type

###### 6.1
A method is declared in a similar way to a function except an extra parameter appears before the function name.
```go
 package geometry
     import "math"
     type Point struct{ X, Y float64 }
     // traditional function
     func Distance(p, q Point) float64 {
         return math.Hypot(q.X-p.X, q.Y-p.Y)
     }
     // same thing, but as a method of the Point type
     func (p Point) Distance(q Point) float64 {
         return math.Hypot(q.X-p.X, q.Y-p.Y)
     }
```
The extra parameter is the method's receiver which is used in go instead of the keyword _this_ or _self_ for the receiver.

To call a method, the following is performed
```go
    p := Point{1, 2}
    q := Point{4, 6}
    p.Distance(p, q)
```
all methods for a given type must have a unique name but method for different types can use the same name for a method

##### 6.2
Methods that must update the receiver, must be attached to the receivers pointer.
Convention dictate that if any method of a receiver has a pointer receiver, all methods of the receiver should a have pointer receiver even the ones that don't require it.
```go
func (p *Point) ScaleBy(factor float64) {
         p.X *= factor
         p.Y *= factor
     }
```
The name of the method is (*Point).ScaleBy
names types and pointers to them are the only types that may appear in a recevier declaration

```go
 type P *int
     func (P) f() { /* ... */ } // compile error: invalid receiver type
```

```go
r := &Point{1, 2}
r.ScaleBy(2)
```
or
```go
p := Point{1, 2}
pptr := &p
pptr.ScaleBy(2)
```
or
```go
p := Point{1, 2}
(&p).ScaleBy(2)
```
but we can also use the following
```go
p.ScaleBy(2)
```
as the compuler will implicitly add the following `&p` to the variable

when adding a type whose methods allow nil as a receiver, it's worth pinting this out explicitly in the documentation
```go
 // An IntList is a linked list of integers.
     // A nil *IntList represents the empty list.
     type IntList struct {
Value int
         Tail  *IntList
     }

// Sum returns the sum of the list elements.
     func (list *IntList) Sum() int {
         if list == nil {
             return 0
      }
      return list.Value + list.Tail.Sum()
}
```
##### 6.3
Composing types by struct embedding
```go
import "image/color"
     type Point struct{ X, Y float64 }
     type ColoredPoint struct {
         Point
         Color color.RGBA
     }
```
Two pertinent examples
```go
    var (
         mu sync.Mutex // guards mapping
         mapping = make(map[string]string)
        )
     func Lookup(key string) string {
         mu.Lock()
         v := mapping[key]
         mu.Unlock()
         return v
    }
```
```go
var cache = struct {
         sync.Mutex
mapping map[string]string }{
         mapping: make(map[string]string),
     }
     func Lookup(key string) string {
         cache.Lock()
         v := cache.mapping[key]
         cache.Unlock()
         return v
}
```
because the sync.Mutex field is embedded within it, its Lock and Unlock methods are promoted to the unnamed struct type allowing us to lock the cache with a self explanatory syntax

##### 6.5
sets are represented by maps with boolean values as values and some data type as keys

##### 6.6
go has a singular way to handle encapsulation which is through capitalization of identifiers. Capitalized identifiers are exported from the package in which they are defined and uncapitalized one aren't. This same mechanismis is applied to fields of a struct or the methods of some type.

An object's variables can only be set by functions in the same Package
---
## Chapter 7
interface type is an abstract type
```go
package fmt
     func Fprintf(w io.Writer, format string, args ...interface{}) (int, error)
     func Printf(format string, args ...interface{}) (int, error) {
         return Fprintf(os.Stdout, format, args...)
}
     func Sprintf(format string, args ...interface{}) string {
         var buf bytes.Buffer
         Fprintf(&buf, format, args...)
         return buf.String()
}

package io
     // Writer is the interface that wraps the basic Write method.
     type Writer interface {
         // Write writes len(p) bytes from p to the underlying data stream.
         // It returns the number of bytes written from p (0 <= n <= len(p))
         // and any error encountered that caused the write to stop early.
         // Write must return a non-nil error if it returns n < len(p).
         // Write must not modify the slice data, even temporarily.
         //
         // Implementations must not retain p.
         Write(p []byte) (n int, err error)
}
```

###### 7.2
Interface Types
```go
    package io
    type Reader interface {
         Read(p []byte) (n int, err error)
    }
    type Closer interface {
         Close() error

    }

    type ReadWriter interface {
         Reader
         Writer }
     type ReadWriteCloser interface {
         Reader
         Writer
         Closer }
```
###### 7.3
When utilizing interfaces, it is important that a given type satisfies an interface in that all the methods the interface requires are present

Abstractions can be expressed as interfaces

```go
 type Artifact interface {
         Title() string
         Creators() []string
         Created() time.Time
     }

 type Text interface {
         Pages() int
Words() int
         PageSize() int
     }
     type Audio interface {
         Stream() (io.ReadCloser, error)
         RunningTime() time.Duration
         Format() string // e.g., "MP3", "WAV"
}
     type Video interface {
         Stream() (io.ReadCloser, error)
         RunningTime() time.Duration
         Format() string // e.g., "MP4", "WMV"
         Resolution() (x, y int)
}
```
Group concrete types based on their shared behavior
We can define new abstractions or groupings of interest when we need them without modifying the declaration of a concrete type

##### 7.5
interfaces can have values called interface value which has 2 components, a concrete type and a value of that type

Can only compare interface values if they are composed of dynamic values of comparable types

Interfaces always have a type and a value. __IMPORTANT__

A nil inteface which contains no values is not the same as an interface value containing a pointer that happens to be nil

##### 7.6
Putting it all together
```go
package sort
     type Interface interface {
         Len() int
         Less(i, j int) bool // i, j are indices of sequence elements
         Swap(i, j int)
     }

type StringSlice []string
     func (p StringSlice) Len() int           { return len(p) }
     func (p StringSlice) Less(i, j int) bool { return p[i] < p[j] }
     func (p StringSlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

sort.Sort(StringSlice(names))
```
Notice type StringSlice implements the necessary methods of interface Interface

##### 7.7
```go
package http
     type Handler interface {
         ServeHTTP(w ResponseWriter, r *Request)
}
     func ListenAndServe(address string, h Handler) error
```
```go
func (db database) ServeHTTP(w http.ResponseWriter, req *http.Request) {
         switch req.URL.Path {
         case "/list":
             for item, price := range db {
                 fmt.Fprintf(w, "%s: %s\n", item, price)
             }
         case "/price":
             item := req.URL.Query().Get("item")
             price, ok := db[item]
             if !ok {
                 w.WriteHeader(http.StatusNotFound) // 404
                 fmt.Fprintf(w, "no such item: %q\n", item)
                 return
}
             fmt.Fprintf(w, "%s\n", price)
         default:
             w.WriteHeader(http.StatusNotFound) // 404
             fmt.Fprintf(w, "no such page: %s\n", req.URL)
         }
}
```
net/http provides a _ServeMux_ which is inherently a request multiplexer to simplify the association between urls and handlers ... it aggregates a collection of http.handlers into a single http.handler

```go
func main() {
         db := database{"shoes": 50, "socks": 5}
         mux := http.NewServeMux()
         mux.Handle("/list", http.HandlerFunc(db.list))
         mux.Handle("/price", http.HandlerFunc(db.price))
         log.Fatal(http.ListenAndServe("localhost:8000", mux))
}
     type database map[string]dollars
     func (db database) list(w http.ResponseWriter, req *http.Request) {
         for item, price := range db {
             fmt.Fprintf(w, "%s: %s\n", item, price)
         }
}
     func (db database) price(w http.ResponseWriter, req *http.Request) {
         item := req.URL.Query().Get("item")
         price, ok := db[item]
         if !ok {
             w.WriteHeader(http.StatusNotFound) // 404
             fmt.Fprintf(w, "no such item: %q\n", item)
             return
}
         fmt.Fprintf(w, "%s\n", price)
     }
```
```go
     mux.HandleFunc("/list", db.list)
     mux.HandleFunc("/price", db.price)
```

```go
  func main() {
         db := database{"shoes": 50, "socks": 5}
         http.HandleFunc("/list", db.list)
         http.HandleFunc("/price", db.price)
         log.Fatal(http.ListenAndServe("localhost:8000", nil))
}
```
##### 7.15
When designing a new package only use interfces if they are satisfied by two or more types