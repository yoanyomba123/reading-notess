# McKinsey & Company
The McKinsey Global Institute (MGI), the business and economics research arm of McKinsey, was established in 1990 to develop a deeper understanding of the evolving global economy. MGI's mission is to provide leaders in the commercial, public, and social sectors with the facts and insights on which to base management and policy decisions.

MGI research combines the disciplines of economics and management, employing the analytical tools of economics with the insights of business leaders. Its "micro-to-macro" methodology examines microeconomic industry trends to better understand the broad macroeconomic forces affecting business strategy and public policy.

MGI's in-depth reports have covered more than 20 countries and 30 industries. Current research focuses on six themes: productivity and growth, natural resources, labor markets, the evolution of global financial markets, the economic impact of technology and innovation, and urbanization. Recent reports have assessed the digital economy, impact of AI and automation on employment, income inequality, the productivity puzzle, the economic benefits of tackling gender inequality, a new era of global competition, Chinese innovation, and digital and financial globalization. The partners of McKinsey fund MGI's research; it is not commissioned by any business, government, or other institution.

The McKinsey Global Institute (MGI) has been named the number one private-sector think tank in the world in the 2018 Global Go to Think Tank Index Report, published on January 31, 2019 by the Think Tank and Civil Society Program at the Lauder Institute, University of Pennsylvania. This is the fourth year in a row that MGI has been awarded this top spot in the global ranking.  

---
## The Social Economy: Unlocking value and productivity through social technologies
### Statistics
- > 1.5 billion social networking users globally
- Proportion of total online users who interact with social networks regularly: 80%
- Proportion of companies using social technologies: 70%
    - Companies obtaining some business benefit from social technologies use: 90%
    - Average time spent per week by knowledge workers writing emails, searching for informations and collaborating: 28 hours
- 900 Billion - 1.3 Trillion dollars in annual value that can be unlocked by social technologies in 4 sectors
    - Share of customer spending that could be influenced by social shopping: 1/3
- Potential value from better enterprise communication and collaboration compared with other social technology benefits: 2x
- Share of companies deriving substancial benefit fro social technologies across all stakeholders: customers, employees, and business partners: 3%
- Potential improvement possible in knowledge worker productivity: 20-25%

### How To Create Value From Social Technologies From A Business Standpoint
Social technologies have the ability to tap the great cognitive surplus provided by society by transforming the purpose of leisure time from consuming to creating content and collaborating. 

Companies are using social technologies to generate better consumer insights at lower cost and at faster rates. Additionally, they are engaging customers on social networking platforms further providing them with unfilthered feedback and behavioral data. Social networks are being utilized to crowdsource product ideas and even co-create new and novel features.  Social platforms have now been found to substantially increase the productivity of knowledge workers. These platforms extend the capabilities of high skill workers by streamlining communication and collaboration, lowering barriers of entry between functional silos, and even redrawing the boundaries of the enterprise to bring in additional knowledge and expertise in "extended networked enterprises". 

social technologies can enable organizations to become fully networked enterprises - networked in both the technical and behavioral sense. 

Several distinct properties of social technologies make then uniquely powerful enablers of value creation. Fundamentally, they endow social interactions with the speed, scale, and economics of the internet. 

----
### Social Technologies 
- Media and File Sharing
- Social Gaming
- Crowd Sourcing
    - harness collective knowledge and generate collectively derived answers
- Shared Work Spaces
    - co-create content and coordinate projects and tasks
- Discussion forums
    - open discussion community
- Wikis
    - search, create, and adapt articles
- Social Commerce
    - purchase in groups on social platforms while sharing opinions
- Ratings and Reviews
    - Rate products, services, and experiences
- Blogs and Microblogs
    - Publish and discuss opinions/experiences
- Social Networks

The web's growth in reach and capability and as a medium of interaction set the stage for the explosive growth of social technologies.

---
### Social Technology Adoption
while it took commercial TV 13 years to reach 50 million households, it took facebook 1 year to reach 50 million users and twitter 9 months. 

Social technology taps the fundamental human behavior which is to seek identity and "connectedness" through affiliations with other individuals and groups that share similar characteristics, interests, and beliefs.  Social technologies have given basic sociological patterns the speed and scale of the internet at virtually zero maginal costs. The technologies have lowered barriers for joining groups, and making social connections. 

Despite the rapid adoption of social technologies, there is far more opportunity ahead. From a survey of 4200 executives at companies around the world, 70% were using social media and 90% of those found business benefits. Yet only 3% of companies could be identified as fully networked meaning that they were achieving substantial benefits from use of these technologies across all parts of the organization and with customers and external partners.

Another indicator of the growth potential of social technologies is the fast-growing share of total time spent on communication and content consumption that takes place on social platforms. 
- Americans spend about 11 hours a day communicating and consuming messages in various ways (in person, TV, reading, email, ...etc0
    - The average american spends about 35 minutes (5%) of his or her total time interacting with content and communicating (which doesnt capture all messaging via social technologies)
    - Compare with 60 minutes for email and 14 miutes for phone
---
### Social Technology Value Creation
Present below are the distinctive properties that make social technologies be enablers of large scale value creation as well as possible explanation to their rapid adoption and high potential impact
* Social is a feature not a product. Social features can be applied to any technology involving interactions among people. 
* Social technologies are enablers of social behaviors endowing such interactions to be performed at scale, speed, as well as providing such interactions with the disruptive economics of the internet. Freed from the physical limitations of the real world, people can connect accross geographies and time zones while multiplying their influence beyond the numbers of people they can normally reach. 
* Social technologies foster content creation, distribution, and consumption. Allowing for an extension of the disintermediating power of the internet to the masses. Such technologies not only change the economics of content creation and distribution but also the nature of content itself allowing it to become an evolving discussion rather than a fixed product.
* The social graph captures the most important information with respect to which group members may contribute the most and withold the greatest levele of influence.
* Social technologies can be disruptive to existing power structures allowing people to connect at a different scale and create a powerful and unified voice. This allows for a more significant impact on the way in which dialogues are shaped and policy is made.
* Social technologies allow for more genuine and timely insights into consumer prefernces and trends while increasing transparency into products, markets, organizations, and institutions.

---
### How Do Social Technologies Add Value In Organizational Functions
#### Organizational Functions
* Product Development
    - Derive Customer Insights
    - Co-create Products With Customers
* Operations and Distributions
    - Leverage Social To Forecast and Monitor
    - Use Social To Distribute Business Processes
* Marketing and Sales
    - Obtain Customer Insights
    - Marketing Communication/Interaction
    - Generate and Foster Sales Leads
    - Social Commerce
* Customer Service
    - Customer Care Via Social Technologies
* Business Support
    - Improve Collaboration and Communication; Match Talent To Tasks
#### Across Entire Enterprise
* Levers
    - Social Tech. To Improve intra and inter organizational comm.
    - Use Social Tech. To match talent to tasks

There are four industries which could be heavily impacted by social technologies. These industries include consumer packaged goods, consumer financial services, professional services, and advanced manufacturing. 
Estimated annual value creation potential is $900 billion to $1.3 trillion. About $345 billion of this value potential would stem from product development, $500 billion from marketing, $230 billion from improvements in business support activities. 

[figure](./Figures/value_creation_across_sectors.png)

Social tech. has the ability to exponentially gain traction due to the many to many communication channels they foster. 

A significant amount of the value unlocked by companies using social tech. eventually will accrue to its consumers either due to market players competing away the present surplus or because social technologies provide the insights that allow customers to obtain goods better suited for their respective needs. Value is captured when these better products increase total demand. 

